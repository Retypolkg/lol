#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:a7c4f231d447de77dfd82f7f074523fb3a184308; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:0a49cc848381c4941cab137719177d89cbf42d63 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:a7c4f231d447de77dfd82f7f074523fb3a184308 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.recovery.updated true
fi
